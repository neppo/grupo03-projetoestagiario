/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package code;

import java.util.ArrayList;


/**
 *
 * @author Richard
 */
public class UsuarioDao{
    
    private ArrayList<Usuario> usuarios = new ArrayList<>();
    
    public UsuarioDao() {
        usuarios.add(new Usuario("teste", "123"));
    }
    
    public Usuario valida(Usuario user) {
        for(Usuario u : usuarios){
            if(user.getNome().equals(u.getNome())){
                if(user.getSenha().equals(u.getSenha())){
                    return user;
                }
            }
        }
        return null;
    }
}
