<%-- 
    Document   : index
    Created on : 29/07/2017, 14:09:05
    Author     : Richard
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Projeto Estágiario</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/materialize.css"  >
        <link rel="stylesheet" href="css/style.css" >
    </head>
    <body class="blue lighten-4">
        <div class="container">

            <div class="row  ">
                <div class="col s5 pull-s1">
                    <img class="responsive-img" src="img/xicara2.png">
                </div>
                <form action="Login" method="POST" class="col s6 cadastro ">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Digite o nome" id="usuario" type="text" class="validate" name="txtid">
                            <label for="usuario">Usuário</label>
                        </div>

                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Digite a senha" id="senha" type="password" class="validate" name="txtsa">
                            <label for="senha">Senha</label>
                        </div>

                    </div>

                    <button class="btn waves-effect waves-light botaologin" type="submit" name="action">Login </button>

                    <a href="cadastrar.html" class="right-align"><p>cadastre-se já!</p></a>
                </form>

            </div>



        </div>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
    </body>
</html>
